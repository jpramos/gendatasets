
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.datasets import make_classification


N_SAMPLES = 10000
NAMES_FILE = 'RAND_DATASET.names'
DATA_FILE = 'RAND_DATASET.data'

def write_to_file(filename, rules):

    with open(filename, 'w') as fd:
        for r in rules:
            fd.write(','.join(map(str, r)))
            fd.write('\n')

def write_namesfile(filename, n):

    with open(filename, 'w') as fd:
        fd.write('class.\t\t\t\t| target attribute\n\n')

        for c in range(n):
            fd.write('x_{}:\t\t\t\tcontinuous.\n'.format(c))

        fd.write('\nclass:\t\t\t\t0, 1.\n') 



X, y = make_classification(n_samples=N_SAMPLES, n_features=2, n_redundant=0, n_informative=2,
        random_state=1, n_clusters_per_class=1, flip_y=0.0, class_sep=2.0)

figure = plt.figure(figsize=(27,9))

x_min, x_max = X[:,0].min() - .5, X[:,0].max()+.5
y_min, y_max = X[:,1].min() - .5, X[:,1].max()+.5

xx, yy = np.meshgrid(np.arange(x_min, x_max, .02), np.arange(y_min, y_max,
    .02))

cm = plt.cm.RdBu
cm_bright = ListedColormap(['#FF0000', '#0000FF'])
ax = plt.subplot(1, 1, 1)
ax.scatter(X[:,0], X[:,1], c=y, cmap=cm_bright)
ax.set_xlim(xx.min(), xx.max())
ax.set_ylim(yy.min(), yy.max())
ax.set_xticks(())
ax.set_yticks(())

write_to_file(DATA_FILE, np.concatenate((X, y[:,np.newaxis]),1))
write_namesfile(NAMES_FILE, 2)

plt.show()


